<?php

namespace App\Exception\Group;

use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class UserAlreadyInGroupException extends ConflictHttpException {

	public const MESSAGE = 'The user to add is already in this group';

	public static function create(): self {
		throw new self(self::MESSAGE);
	}
}
