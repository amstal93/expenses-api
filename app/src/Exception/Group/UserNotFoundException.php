<?php

namespace App\Exception\Group;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserNotFoundException extends BadRequestHttpException {

	public const MESSAGE = 'The user is not found';

	public static function create(): self {
		throw new self(self::MESSAGE);
	}
}
