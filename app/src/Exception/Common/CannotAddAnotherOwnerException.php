<?php

namespace App\Exception\Common;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CannotAddAnotherOwnerException extends AccessDeniedHttpException {

	private const MESSAGE = 'You can\'t add another user as owner';

	public static function create(): self {
		throw new self(self::MESSAGE);
	}
}
