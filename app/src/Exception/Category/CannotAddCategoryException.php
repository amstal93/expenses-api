<?php

namespace App\Exception\Category;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CannotAddCategoryException extends AccessDeniedHttpException {

	public const MESSAGE = 'You cannot add this category to this expense';

	public static function create(): self {
		throw new self(self::MESSAGE);
	}
}
