<?php

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Category;
use App\Entity\Expense;
use App\Entity\Group;
use App\Entity\User;
use App\Repository\GroupRepository;
use App\Security\Role;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class DoctrineUserExtension implements QueryCollectionExtensionInterface {

	private TokenStorageInterface $tokenStorage;
	private Security $security;
	private GroupRepository $groupRepository;

	public function __construct(
		TokenStorageInterface $tokenStorage,
		Security $security,
		GroupRepository $groupRepository) {

		$this->tokenStorage = $tokenStorage;
		$this->security = $security;
		$this->groupRepository = $groupRepository;

	}

	/**
	 * Doctrine ORM query extensions for collection queries implement
	 *
	 * @param QueryBuilder $queryBuilder
	 * @param QueryNameGeneratorInterface $queryNameGenerator
	 * @param string $resourceClass
	 * @param string|null $operationName
	 */
	public function applyToCollection(
		QueryBuilder $queryBuilder,
		QueryNameGeneratorInterface $queryNameGenerator,
		string $resourceClass,
		string $operationName = NULL) {

		$this->addWhere($queryBuilder, $resourceClass);

	}

	/**
	 * Limit the case when a user try to get other user info for security
	 * reasons
	 *
	 * @param QueryBuilder $queryBuilder
	 * @param string $resourceClass
	 */
	private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void {

		// if is an admin let get all info
		if ($this->security->isGranted(Role::ROLE_ADMIN)) {
			return;
		}

		/** @var User $user */
		$user = $this->tokenStorage->getToken()->getUser();

		// table
		$rootAlias = $queryBuilder->getRootAliases()[0];

		// in the case of groups, limit to other users get information from other
		// user groups
		if ($resourceClass === Group::class) {
			$where =
				sprintf(
					'%s.%s = :currentUser',
					$rootAlias,
					$this->getResources()[$resourceClass]
				);
			$queryBuilder->andWhere($where);
			$queryBuilder->setParameter(':currentUser', $user);
		}

		// with categories and expenses limit to get only owned categories or expenses
		// or owned by the group
		if ($resourceClass === Category::class || $resourceClass === Expense::class) {
			$parameterId = '';
			if ($queryBuilder->getParameters()[0] !== null) {
				$parameterId = $queryBuilder->getParameters()[0]->getValue();
			}

			if ($this->isGroupAndUserIsMember($parameterId, $user)) {
				$queryBuilder->andWhere(sprintf('%s.group = :parameterId', $rootAlias));
				$queryBuilder->setParameter(':parameterId', $parameterId);
			}
			else {
				$queryBuilder->andWhere(sprintf('%s.%s = :currentUser', $rootAlias, $this->getResources()[$resourceClass]));
				$queryBuilder->andWhere(
					sprintf(
						'%s.group is null',
						$rootAlias
					)
				);
				$queryBuilder->setParameter(':currentUser', $user);
			}
		}
	}

	/**
	 * Get possibles foreign key names for the same field in different tables
	 *
	 * @return array|string[]
	 */
	private function getResources(): array {
		return [
			Group::class => 'owner',
			Category::class => 'user',
			Expense::class => 'user'
		];
	}

	/**
	 * Check if exists a group given an id and checks if the user given is member of
	 * this group
	 *
	 * @param string $parameterId
	 * @param User $user
	 *
	 * @return bool
	 */
	private function isGroupAndUserIsMember (string $parameterId, User $user): bool {

		$group = $this->groupRepository->findOneById($parameterId);

		if ($group !== null) {
			return $this->groupRepository->userIsMember($group, $user);
		}

		return false;
	}
}
