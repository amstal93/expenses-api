<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Expense;
use App\Entity\Group;
use App\Entity\User;
use App\Security\Role;
use App\Service\Password\EncoderService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture {

	private EncoderService $encoderService;

	public function __construct(EncoderService $encoderService) {
		$this->encoderService = $encoderService;
	}

	/**
	 * @param ObjectManager $manager
	 *
	 * @throws \Exception
	 */
	public function load(ObjectManager $manager) {
    $users = $this->getUsers();

    foreach ($users as $userData) {
    	$user = new User($userData['name'], $userData['email'], $userData['id']);

    	$user->setPassword(
    		$this->encoderService->generateEncodedPasswordForUser(
    			$user,
			    $userData['password']
		    )
	    );

    	$user->setRoles($userData['roles']);
    	$user->setActive($userData['active']);

    	$manager->persist($user);

	    foreach ($userData['categories'] as $categoryData) {
		    $category =
			    new Category($categoryData['name'], $user, null, $categoryData['id']);
		    $manager->persist($category);

		    foreach ($categoryData['expenses'] as $expenseData) {
			    $expense = new Expense(
				    $category,
				    $user,
				    $expenseData['amount'],
				    $expenseData['description'],
				    null,
				    $expenseData['id']
			    );

			    $manager->persist($expense);
		    }
	    }

    	foreach ($userData['groups'] as $groupData) {
    		$group = new Group($groupData['name'], $user, $groupData['id']);

				$group->addUser($user);
		    $manager->persist($group);

		    foreach ($groupData['categories'] as $categoryData) {

			    $category =
				    new Category($categoryData['name'], $user, $group, $categoryData['id']);

			    $manager->persist($category);

			    foreach ($categoryData['expenses'] as $expenseData) {
				    $expense = new Expense(
					    $category,
					    $user,
					    $expenseData['amount'],
					    $expenseData['description'],
					    $group,
					    $expenseData['id']
				    );

				    $manager->persist($expense);
			    }
		    }
	    }
    }

    $manager->flush();
	}

	/**
	 * Aux function to generate test users
	 *
	 * @return array|array[]
	 */
	private function getUsers(): array {

	  return [
	    [
				'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57001',
		    'name' => 'Admin',
		    'email' => 'admin@admin.com',
		    'password' => '1234',
		    'active' => true,
		    'roles' => [
		      Role::ROLE_ADMIN,
			    Role::ROLE_USER
		    ],
		    'groups' => [
		    	[
		    		'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57003',
				    'name' => 'Admin\'s group',
				    'categories' => [
				    	[
						    'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57007',
						    'name' => 'Admin\'s group category',
						    'expenses' => [
							    [
								    'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57011',
								    'amount' => 300,
								    'description' => 'Admin group category expense'
							    ]
						    ]
					    ]
				    ],
			    ]
		    ],
				'categories' => [
					[
						'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57005',
						'name' => 'Admin\'s category',
						'expenses' => [
							[
								'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57009',
								'amount' => 100,
								'description' => 'Admin category expense'
							]
						]
					]
				]
	    ],
	    [
		    'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57002',
		    'name' => 'User',
		    'email' => 'user@user.com',
		    'password' => '5678',
		    'active' => true,
		    'roles' => [
			    Role::ROLE_USER
		    ],
		    'groups' => [
			    [
				    'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57004',
				    'name' => 'User\'s group',
				    'categories' => [
				    	[
						    'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57008',
						    'name' => 'User\'s group category',
						    'expenses' => [
							    [
								    'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57012',
								    'amount' => 400,
								    'description' => 'User group category expense'
							    ]
						    ]
					    ]
				    ]
			    ]
		    ],
		    'categories' => [
			    [
				    'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57006',
				    'name' => 'User\'s category',
				    'expenses' => [
					    [
						    'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57010',
						    'amount' => 200,
						    'description' => 'User category expense'
					    ]
				    ]
			    ]
		    ]
	    ],
		  [
			  'id' => 'f6133ded-46a1-434d-92e3-7a44a3a57013',
			  'name' => 'User Not Activated',
			  'email' => 'user-no-active@user.com',
			  'password' => '1234',
			  'active' => false,
			  'roles' => [
				  Role::ROLE_USER
			  ],
			  'groups' => [],
			  'categories' => []
		  ]
	  ];
	}

}
