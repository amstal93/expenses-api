<?php

namespace App\Service\Password;

use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class EncoderService {

	private EncoderFactoryInterface $encoderFactory;

	public function __construct(EncoderFactoryInterface $encoderFactory) {
		$this->encoderFactory = $encoderFactory;
	}

	/**
	 * Generate a encrypted password from an user and plain password
	 *
	 * @param UserInterface $user
	 * @param string $password
	 * @param string|null $salt
	 *
	 * @return string encrypted password
	 */
	public function generateEncodedPasswordForUser(UserInterface $user, string $password, string $salt = null): string {
		$encoder = $this->encoderFactory->getEncoder($user);
		return $encoder->encodePassword($password, $salt);
	}
}
