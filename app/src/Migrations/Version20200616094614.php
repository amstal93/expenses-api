<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200616094614 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'add avatar field to user table';
    }

    public function up(Schema $schema) : void
    {
	    $this->addSql('
          alter table user add column avatar varchar(255) default null after active 
        ');

    }

    public function down(Schema $schema) : void
    {
	    $this->addSql('
          alter table user drop column avatar
        ');

    }
}
