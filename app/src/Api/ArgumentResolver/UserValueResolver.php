<?php

namespace App\Api\ArgumentResolver;

use App\Entity\User;
use App\Repository\UserRepository;
use Generator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class UserValueResolver implements ArgumentValueResolverInterface {

	private const AVATAR_UPLOAD_ROUTE = 'api_users_upload_avatar_item';
	private const AVATAR_REMOVE_ROUTE = 'api_users_remove_avatar_item';

	private TokenStorageInterface $tokenStorage;
	private UserRepository $userRepository;

	public function __construct(TokenStorageInterface $tokenStorage, UserRepository $userRepository) {
		$this->tokenStorage = $tokenStorage;
		$this->userRepository = $userRepository;
	}

	/**
	 * @inheritDoc
	 */
	public function supports(Request $request, ArgumentMetadata $argument): bool {
		if ($argument->getType() !== User::class) {
			// if is not an user then exist
			return false;
		}

		$token = $this->tokenStorage->getToken();

		if (!$token instanceof TokenInterface) {
			// security check
			return false;
		}

		// if the token user is not a user return false
		return $token->getUser() instanceof User;
	}

	/**
	 * Resolve a user constructor param getting him from the token data
	 * or in the upload an avatar from the param id
	 *
	 * @inheritDoc
	 */
	public function resolve(Request $request, ArgumentMetadata $argument): Generator {

		// there is a problem when try to edit another user avatar because
		// an user argument always is resolved with the token user and
		// finally change your own avatar, is better return the user to pretend
		// to change and give an exception in the userPreWriteListener
		if ($request->get('_route') === self::AVATAR_UPLOAD_ROUTE ||
		    $request->get('_route') === self::AVATAR_REMOVE_ROUTE) {
			$id = $request->get('id');

			if ($id !== null) {
				yield $this->userRepository->findOneById($id);
			}
		}
		else {
			// int the rest of cases return the authenticated user
			/** @var User $user */
			$user = $this->tokenStorage->getToken()->getUser();
			yield $this->userRepository->findOneByEmail($user->getEmail());
		}
	}
}
