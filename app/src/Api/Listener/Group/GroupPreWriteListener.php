<?php

namespace App\Api\Listener\Group;

use App\Api\Listener\PreWriteListener;
use App\Entity\Group;
use App\Entity\User;
use App\Exception\Common\CannotAddAnotherOwnerException;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class GroupPreWriteListener implements PreWriteListener {

	private const POST_GROUP = 'api_groups_post_collection';

	private TokenStorageInterface $tokenStorage;

	public function __construct(TokenStorageInterface $tokenStorage) {
		$this->tokenStorage = $tokenStorage;
	}

	public function onKernelView(ViewEvent $event): void {

		$request = $event->getRequest();

		// create a group
		if ($request->get('_route') === self::POST_GROUP) {
			/** @var User $tokenUser */
			$tokenUser = $this->tokenStorage->getToken()->getUser();

			/** @var Group $group */
			$group = $event->getControllerResult();

			// check if the group is owned bu the logged user
			if(!$group->isOwnedBy($tokenUser)) {
				throw CannotAddAnotherOwnerException::create();
			}

			$group->addUser($tokenUser);
		}
	}
}
