<?php

namespace App\Api\Action\User;

use App\Entity\User;
use App\Service\File\ImageUploaderService;
use Symfony\Component\HttpFoundation\Request;

class UploadAvatar {

	private ImageUploaderService $uploaderService;

	public function __construct(ImageUploaderService $uploaderService) {
		$this->uploaderService = $uploaderService;
	}

	/**
	 * Upload an avatar file to create it or update it if exists
	 *
	 * @param Request $request
	 * @param User $user
	 *
	 * @return User
	 * @throws \Exception
	 */
	public function __invoke(Request $request, User $user): User {
		return $this->uploaderService->uploadAvatar($request, $user);
	}

}
