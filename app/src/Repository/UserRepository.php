<?php

namespace App\Repository;

use App\Entity\User;

class UserRepository extends BaseRepository {

	/**
	 * @inheritDoc
	 */
	protected static function entityClass(): string {
		return User::class;
	}

	/**
	 * Find an user by id
	 *
	 * @param string $id
	 *
	 * @return User|null
	 */
	public function findOneById(string $id): ?User {

		/** @var User $user */
		$user = $this->objectRepository->find($id);

		return $user;

	}

	/**
	 * Find an user given an email, if not is found then return null
	 *
	 * @param string $email
	 *
	 * @return User|null
	 */
	public function findOneByEmail(string $email): ?User {

		/** @var User $user */
		$user = $this->objectRepository->findOneBy(['email' => $email]);

		return $user;
	}

	/**
	 * Get all users
	 *
	 * @return User[]
	 */
	public function findAll(): array {
		return $this->objectRepository->findAll();
	}

	/**
	 * Get all users count
	 *
	 * @return int count
	 */
	public function getUsersCount(): int {
		$users = $this->objectRepository->findAll();
		return count($users);
	}

	/**
	 * Persist an user given
	 *
	 * @param User $user
	 */
	public function save(User $user): void {
		$this->saveEntity($user);
	}

}
