<?php
declare(strict_types=1);

namespace App\Security\Authorization\Voter;

use App\Entity\Expense;
use App\Entity\User;
use App\Security\Role;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ExpenseVoter extends BaseVoter {

	private const EXPENSE_READ = 'EXPENSE_READ';
	private const EXPENSE_CREATE = 'EXPENSE_CREATE';
	private const EXPENSE_UPDATE = 'EXPENSE_UPDATE';
	private const EXPENSE_DELETE = 'EXPENSE_DELETE';
	private const EXPENSE_GROUP_NOTIFICATION = 'EXPENSE_GROUP_NOTIFICATION';

	/**
	 * @inheritDoc
	 */
	protected function supports(string $attribute, $subject): bool {
		return in_array($attribute, $this->getSupportedAttributes(), true);
	}

	/**
	 * Checks if the current user is an admin or the expense is owned by the user,
	 * group or is member of this group
	 *
	 * @param string $attribute
	 * @param Expense|null $subject
	 * @param TokenInterface $token
	 *
	 * @return bool|void
	 */
	protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool {

		if (self::EXPENSE_CREATE === $attribute) {
			return true;
		}

		/** @var User $tokenUser */
		$tokenUser = $token->getUser();

		if (self::EXPENSE_READ === $attribute) {
			if ($subject === null) {
				return $this->security->isGranted(Role::ROLE_ADMIN);
			}

			if ($subject->getGroup() !== null) {
				return $this->security->isGranted(Role::ROLE_ADMIN) ||
				       $this->groupRepository->userIsMember($subject->getGroup(), $tokenUser);
			}

			return $this->security->isGranted(Role::ROLE_ADMIN) ||
			       $subject->isOwnedByUser($tokenUser);
		}

		if (self::EXPENSE_UPDATE === $attribute || self::EXPENSE_DELETE === $attribute) {
			if ($subject->getGroup() !== null) {
				return $this->security->isGranted(Role::ROLE_ADMIN) ||
				       $this->groupRepository->userIsMember($subject->getGroup(), $tokenUser);
			}

			return $this->security->isGranted(Role::ROLE_ADMIN) ||
			       $subject->isOwnedByUser($tokenUser);
		}

		// only members of a group can send notifications
		if (self::EXPENSE_GROUP_NOTIFICATION === $attribute) {

			// by the moment only allow notification to groups
			if ($subject->getGroup() === null) {
				return false;
			}

			return $this->groupRepository->userIsMember($subject->getGroup(), $tokenUser);
		}

		return false;
	}

	private function getSupportedAttributes(): array {
		return [
			self::EXPENSE_CREATE,
			self::EXPENSE_READ,
			self::EXPENSE_UPDATE,
			self::EXPENSE_DELETE,
			self::EXPENSE_GROUP_NOTIFICATION
		];
	}
}
