<?php

namespace App\Tests\Functional\Api\Category;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class PutCategoryTest extends CategoryTestBase {

	/**
	 * Test change a owned category
	 */
	public function testPutCategory(): void {
		$payload = ['name' => 'new name'];

		$response = $this->makeRequest(self::IDS['user_category_id'], $payload);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertEquals($payload['name'], $responseData['name']);
	}

	/**
	 * Test forbidden access to change another user category
	 */
	public function testPutAnotherUserGroup(): void {
		$payload = ['name' => 'new name'];

		$response = $this->makeRequest(self::IDS['admin_category_id'], $payload);

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Make a put request given an id and a payload
	 *
	 * @param string $id
	 * @param array  $payload
	 *
	 * @return Response
	 */
	private function makeRequest(string $id,array $payload): Response {
		self::$user->request(
			'PUT',
			sprintf(
				'%s/%s.%s',
				$this->endpoint,
				$id,
				self::FORMAT
			),
			[],
			[],
			[],
			json_encode($payload)
		);

		return self::$user->getResponse();
	}

}
