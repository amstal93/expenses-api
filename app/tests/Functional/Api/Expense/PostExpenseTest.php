<?php

namespace App\Tests\Functional\Api\Expense;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class PostExpenseTest extends ExpenseTestBase {

	/**
	 * Test create an expense without group
	 */
	public function testCreateExpense(): void {
		$payload = [
			'category' => sprintf('/api/v1/categories/%s', self::IDS['admin_category_id']),
			'user' => sprintf('/api/v1/users/%s', self::IDS['admin_id']),
			'amount' => 100.10
		];

		$response = $this->makeRequestWithAdmin($payload);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_CREATED, $response->getStatusCode());
		$this->assertEquals($payload['amount'], $responseData['amount']);
		$this->assertEquals($payload['user'], $responseData['user']);
		$this->assertEquals($payload['category'], $responseData['category']);
	}

	/**
	 * Test forbidden access to create an expense with another user category
	 */
	public function testCreateExpenseWithAnotherUserCategory(): void {
		$payload = [
			'category' => sprintf('/api/v1/categories/%s', self::IDS['user_category_id']),
			'user' => sprintf('/api/v1/users/%s', self::IDS['admin_id']),
			'amount' => 100.10
		];

		$response = $this->makeRequestWithAdmin($payload);

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Test create an expense with an invalid amount
	 */
	public function testCreateExpenseWithInvalidAmount(): void {
		$payload = [
			'category' => sprintf('/api/v1/categories/%s', self::IDS['admin_category_id']),
			'user' => sprintf('/api/v1/users/%s', self::IDS['admin_id']),
			'amount' => 'abcd'
		];

		$response = $this->makeRequestWithAdmin($payload);

		$this->assertEquals(JsonResponse::HTTP_BAD_REQUEST, $response->getStatusCode());
	}

	/**
	 * Test create an expense with group
	 */
	public function testCreateExpenseForGroup(): void {
		$payload = [
			'category' => sprintf('/api/v1/categories/%s', self::IDS['admin_group_category_id']),
			'user' => sprintf('/api/v1/users/%s', self::IDS['admin_id']),
			'group' => sprintf('/api/v1/groups/%s', self::IDS['admin_group_id']),
			'amount' => 100.10
		];

		$response = $this->makeRequestWithAdmin($payload);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_CREATED, $response->getStatusCode());
		$this->assertEquals($payload['amount'], $responseData['amount']);
		$this->assertEquals($payload['user'], $responseData['user']);
		$this->assertEquals($payload['category'], $responseData['category']);
		$this->assertEquals($payload['group'], $responseData['group']);
	}

	/**
	 * Test forbidden access to create an expense for another user group
	 */
	public function testCreateAnotherUserExpenseForGroup(): void {
		$payload = [
			'category' => sprintf('/api/v1/categories/%s', self::IDS['user_group_category_id']),
			'user' => sprintf('/api/v1/users/%s', self::IDS['admin_id']),
			'group' => sprintf('/api/v1/groups/%s', self::IDS['user_group_id']),
			'amount' => 100.10
		];

		$response = $this->makeRequestWithAdmin($payload);

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Make a post request with admin user
	 *
	 * @param $payload
	 *
	 * @return Response
	 */
	private function makeRequestWithAdmin($payload): Response {
		self::$admin->request(
			'POST',
			sprintf('%s.%s', $this->endpoint, self::FORMAT),
			[],[],[],
			json_encode($payload)
		);

		return self::$admin->getResponse();
	}

}
