<?php

namespace App\Tests\Functional\Api\Expense;

use App\Tests\Functional\TestBase;
use Doctrine\ORM\Tools\ToolsException;

class ExpenseTestBase extends TestBase {

	protected string $endpoint;

	/**
	 * @throws ToolsException
	 */
	public function setUp(): void {
		parent::setUp();
		$this->endpoint = '/api/v1/expenses';
	}
}
