<?php

namespace App\Tests\Functional\Api\Group;

use Symfony\Component\HttpFoundation\JsonResponse;

class GetGroupCategories extends GroupTestBase {

	public function testGetGroupCategories(): void {
		self::$user->request(
			'GET',
			sprintf('%s/%s/categories.%s', $this->endPoint, self::IDS['user_group_id'], self::FORMAT)
		);

		$response = self::$user->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertCount(1, $responseData['hydra:member']);
	}

	public function testGetAnotherGroupCategories(): void {
		self::$user->request(
			'GET',
			sprintf('%s/%s/categories.%s', $this->endPoint, self::IDS['admin_group_id'], self::FORMAT)
		);

		$response = self::$user->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertCount(0, $responseData['hydra:member']);
	}
}
