<?php

namespace App\Tests\Functional\Api\Group;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class PostGroupTest extends GroupTestBase {

	private const NAME = 'GroupName';

	/**
	 * Test create a group
	 */
	public function testCreateGroup(): void {
		$response = $this->makeRequest(self::IDS['admin_id']);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_CREATED, $response->getStatusCode());
		$this->assertEquals(self::NAME, $responseData['name']);
	}

	/**
	 * Test forbidden access to create a group for another user
	 */
	public function testCreateGroupForAnotherUser(): void {
		$response = $this->makeRequest(self::IDS['user_id']);

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Make a post request given an id
	 *
	 * @param $id
	 *
	 * @return Response
	 */
	private function makeRequest($id): Response {
		$payload = [
			'name' => self::NAME,
			'owner' => sprintf('/api/v1/users/%s', $id)
		];

		self::$admin->request(
			'POST',
			sprintf('%s.%s', $this->endPoint, self::FORMAT),
			[],
			[],
			[],
			json_encode($payload)
		);

		return self::$admin->getResponse();
	}

}
