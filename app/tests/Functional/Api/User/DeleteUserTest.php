<?php

namespace App\Tests\Functional\Api\User;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DeleteUserTest extends UserTestBase {

	/**
	 * Test delete an user with an admin user
	 */
	public function testDeleteUserWithAdmin(): void {
		$response = $this->makeRequest(self::$admin, self::IDS['user_id']);

		$this->assertEquals(JsonResponse::HTTP_NO_CONTENT, $response->getStatusCode());
	}

	/**
	 * Test forbidden access to delete an user with an normal user
	 */
	public function testDeleteAdminWithUser(): void {
		$response = $this->makeRequest(self::$user, self::IDS['admin_id']);

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN,$response->getStatusCode());
	}

	/**
	 * Make a delete request given a KernelBrowser and an id
	 *
	 * @param $browser
	 * @param string $id
	 *
	 * @return Response
	 */
	private function makeRequest(KernelBrowser $browser, string $id): Response {
		$browser->request(
			'DELETE',
			sprintf(
				'%s/%s.%s',
				$this->endpoint,
				$id,
				self::FORMAT
			)
		);

		return $browser->getResponse();
	}
}
