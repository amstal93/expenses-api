<?php

namespace App\Tests\Unit\Security\Validator\Role;

use App\Exception\Role\UnsupportedRoleException;
use App\Security\Role;
use App\Security\Validator\Role\AreValidRoles;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class AreValidRolesTest extends TestCase {

	private AreValidRoles $validator;

	/**
	 * @inheritDoc
	 */
	public function setUp(): void {
		$this->validator = new AreValidRoles();
	}

	/**
	 * Test when all roles given are valid
	 */
	public function testRolesAreValid(): void {

		$payload = [
			'roles' => [
				Role::ROLE_USER,
				Role::ROLE_ADMIN
			]
		];

		$request = new Request(
			[], [], [], [], [], [],
			json_encode($payload)
		);

		$response = $this->validator->validate($request);

		$this->assertIsArray($response);
	}

	/**
	 * Test when a role isn't a valid role
	 */
	public function testRolesNotValid(): void {

		$payload = [
			'roles' => [
				Role::ROLE_USER,
				'ROLE_DUMMY'
			]
		];

		$request = new Request(
			[], [], [], [], [], [],
			json_encode($payload)
		);

		$this->expectException(UnsupportedRoleException::class);
		$this->expectExceptionMessage(
			sprintf('Unsupported role %s',$payload['roles'][1])
		);

		$this->validator->validate($request);
	}
}
