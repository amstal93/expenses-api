<?php

namespace App\Tests\Unit\Api\Service\Group;

use App\Entity\Group;
use App\Entity\User;
use App\Exception\Group\CannotManageGroupException;
use App\Exception\Group\IncorrectGroupException;
use App\Exception\Group\UserAlreadyInGroupException;
use App\Exception\Group\UserIsNotInGroupException;
use App\Exception\Group\UserNotFoundException;
use App\Service\Group\GroupService;
use App\Tests\Unit\TestBase;

class GroupServiceTest extends TestBase {

	private GroupService $groupService;
	private string $group_id;
	private string $user_id;
	private User $user;
	private User $newUser;
	private Group $group;

	/**
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();

		$this->groupService =
			new GroupService($this->userRepository, $this->groupRepository);

		$this->group_id = 'group_01';
		$this->user_id = 'user_to_add_01';
		$this->user = new User('user', 'mail@mail.com');
		$this->newUser = new User('new_user', 'mail2@mail.com');
		$this->group = new Group('group_01', $this->user);
	}

	/**
	 * Test add user to group method
	 *
	 * @throws \Exception
	 */
	public function testAddUserToGroup(): void {

		$this->groupRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->group_id)
			->willReturn($this->group);

		$this->userRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->user_id)
			->willReturn($this->newUser);

		$this->groupRepository
			->expects(self::exactly(2))
			->method('userIsMember')
			->withConsecutive([$this->group, $this->user], [$this->group, $this->newUser])
			->willReturnOnConsecutiveCalls(true, false);

		$this->groupRepository
			->expects(self::exactly(1))
			->method('save')
			->with($this->group);

		$this->groupService->addUserToGroup(
			$this->group_id,
			$this->user_id,
			$this->user
		);
	}

	/**
	 * Test Incorrect group exception when try to add a user to another user group
	 *
	 * @throws \Exception
	 */
	public function testAddUserToGroupIncorrectGroup(): void {

		$this->groupRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->group_id)
			->willReturn(null);

		$this->expectException(IncorrectGroupException::class);
		$this->expectExceptionMessage(IncorrectGroupException::MESSAGE);

		$this->groupService->addUserToGroup(
			$this->group_id,
			$this->user_id,
			$this->user
		);
	}

	/**
	 * Test user to add not found exception
	 *
	 * @throws \Exception
	 */
	public function testAddUserToGroupUserNotFound(): void {

		$this->groupRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->group_id)
			->willReturn($this->group);

		$this->userRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->user_id)
			->willReturn(null);

		$this->expectException(UserNotFoundException::class);
		$this->expectExceptionMessage(UserNotFoundException::MESSAGE);

		$this->groupService->addUserToGroup(
			$this->group_id,
			$this->user_id,
			$this->user
		);
	}

	/**
	 * Test cannot manage this group exception when try to add a user and the logged
	 * user is not in the group
	 *
	 * @throws \Exception
	 */
	public function testAddUserToGroupCannotManageGroup(): void {

		$this->groupRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->group_id)
			->willReturn($this->group);

		$this->userRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->user_id)
			->willReturn($this->newUser);

		$this->groupRepository
			->expects(self::exactly(1))
			->method('userIsMember')
			->with($this->group, $this->user)
			->willReturn(false);

		$this->expectException(CannotManageGroupException::class);
		$this->expectExceptionMessage(CannotManageGroupException::MESSAGE);

		$this->groupService->addUserToGroup(
			$this->group_id,
			$this->user_id,
			$this->user
		);
	}

	/**
	 * Test add user that is already added to the group exception
	 *
	 * @throws \Exception
	 */
	public function testAddUserToGroupUserAlreadyInGroup(): void {

		$this->groupRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->group_id)
			->willReturn($this->group);


		$this->userRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->user_id)
			->willReturn($this->newUser);

		$this->groupRepository
			->expects(self::exactly(2))
			->method('userIsMember')
			->withConsecutive([$this->group, $this->user], [$this->group, $this->newUser])
			->willReturnOnConsecutiveCalls(true, true);

		$this->expectException(UserAlreadyInGroupException::class);
		$this->expectExceptionMessage(UserAlreadyInGroupException::MESSAGE);

		$this->groupService->addUserToGroup(
			$this->group_id,
			$this->user_id,
			$this->user
		);
	}


	/**
	 * Test remove a user from a group
	 *
	 * @throws \Exception
	 */
	public function testRemoveUserFromGroup(): void {

		$this->groupRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->group_id)
			->willReturn($this->group);

		$this->userRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->user_id)
			->willReturn($this->newUser);

		$this->groupRepository
			->expects(self::exactly(2))
			->method('userIsMember')
			->withConsecutive([$this->group, $this->user], [$this->group, $this->newUser])
			->willReturnOnConsecutiveCalls(true, true);

		$this->groupRepository
			->expects(self::exactly(1))
			->method('save')
			->with($this->group);

		$this->groupService->removeUserFromGroup(
			$this->group_id,
			$this->user_id,
			$this->user
		);
	}

	/**
	 * Test user to remove  is not in the group exception
	 *
	 * @throws \Exception
	 */
	public function testRemoveUserFromGroupUserIsNotInGroup(): void {

		$this->groupRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->group_id)
			->willReturn($this->group);

		$this->userRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with($this->user_id)
			->willReturn($this->newUser);

		$this->groupRepository
			->expects(self::exactly(2))
			->method('userIsMember')
			->withConsecutive([$this->group, $this->user], [$this->group, $this->newUser])
			->willReturnOnConsecutiveCalls(true, false);

		$this->expectException(UserIsNotInGroupException::class);
		$this->expectExceptionMessage(UserIsNotInGroupException::MESSAGE);

		$this->groupService->removeUserFromGroup(
			$this->group_id,
			$this->user_id,
			$this->user
		);
	}
}
