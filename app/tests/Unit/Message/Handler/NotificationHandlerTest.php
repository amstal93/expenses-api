<?php

namespace App\Tests\Unit\Message\Handler;

use App\Message\Handler\NotificationHandler;
use App\Message\Notification\Notification;
use App\Message\NotificationText;
use PHPUnit\Framework\TestCase;

class NotificationHandlerTest extends TestCase {

	private NotificationHandler $handler;
	private Notification $notification;

	/**
	 * @inheritDoc
	 */
	public function setUp(): void {
	 $users = [
			'mail1@mail.com',
			'mail2@mail.com'
		];

		$this->notification =
			new Notification(NotificationText::$NOTIFICATION_USER_REGISTERED, $users);

		$this->handler = new NotificationHandler();
	}

	/**
	 * Test that notification handler is invoked
	 */
	public function testMailFunctionCalled(): void {
		$this->assertIsArray($this->handler->__invoke($this->notification));
	}
}
